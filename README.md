# ssrf pwnabot
discover secret services and pwn this bot with ssrf

env example:

WEBHOOK_SERVICE_URL=your_heroku_url  
TELEGRAM_API_TOKEN=your_token  
PORT=8083  
SERVICE_PORT=8080  
DEBUG=true  
USE_WEBHOOK=false  
SECRET_URL=url_with_a_flag_image.png  
